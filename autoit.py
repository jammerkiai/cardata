#!/usr/bin/python3

import requests
import pymysql
import json
import requests
import sys
import xmltodict
from threading import Thread
from pprint import pprint

url="http://ws1.Bilregister.dk/bilregisterservice.asmx?WSDL"
headers = {'content-type': 'text/xml'}
# headers = {'content-type': 'application/soap+xml'}


def getBrands(url, headers):
	
	body = """<?xml version="1.0" encoding="utf-8"?>
	<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	  <soap:Body>
	    <getFabrikaterByTypeId xmlns="http://tempuri.org/">
	      <SystemudbyderId>2b007683-30b7-45c7-a2bc-7363183f8723</SystemudbyderId>
	      <TypeId>1</TypeId>
	    </getFabrikaterByTypeId>
	  </soap:Body>
	</soap:Envelope>"""

	response = requests.post(url,data=body,headers=headers)
	brands = xmltodict.parse(response.content)['soap:Envelope']['soap:Body']['getFabrikaterByTypeIdResponse']['getFabrikaterByTypeIdResult']['BilFabrikat']
	return brands


def getModels(url, headers, brandid, year):
	body ='<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><getModellerByTypeIdFabrikatIdAargang xmlns="http://tempuri.org/"><SystemudbyderId>2b007683-30b7-45c7-a2bc-7363183f8723</SystemudbyderId><TypeId>1</TypeId><FabrikatId>{0}</FabrikatId><Aargang>{1}</Aargang></getModellerByTypeIdFabrikatIdAargang></soap:Body></soap:Envelope>'.format(brandid, year)
	response = requests.post(url,data=body,headers=headers)
	models = xmltodict.parse(response.content)['soap:Envelope']['soap:Body']['getModellerByTypeIdFabrikatIdAargangResponse']['getModellerByTypeIdFabrikatIdAargangResult']
	if (models):
		return models['BilModel']
	else:
		return []

def getMotors(url, headers, modelid, year):
	body = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><getMotorstoerrelserByTypeIdModelIdAargang xmlns="http://tempuri.org/"><SystemudbyderId>2b007683-30b7-45c7-a2bc-7363183f8723</SystemudbyderId><TypeId>1</TypeId><ModelId>{0}</ModelId><Aargang>{1}</Aargang></getMotorstoerrelserByTypeIdModelIdAargang></soap:Body></soap:Envelope>'.format(modelid, year)
	response = requests.post(url,data=body,headers=headers)
	
	motors = xmltodict.parse(response.content)['soap:Envelope']['soap:Body']['getMotorstoerrelserByTypeIdModelIdAargangResponse']['getMotorstoerrelserByTypeIdModelIdAargangResult']
	if (motors):
		return motors['BilMotorstoerrelse']
	else:
		return []


def getTrims(url, headers, modelid, motorid, year):
	body = '<?xml version="1.0" encoding="utf-8"?><soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope"><soap12:Body><getVarianterByTypeIdModelIdAargangMotorstoerrelse xmlns="http://tempuri.org/"><SystemudbyderId>2b007683-30b7-45c7-a2bc-7363183f8723</SystemudbyderId><TypeId>1</TypeId><ModelId>{0}</ModelId><Aargang>{1}</Aargang><Motorstoerrelse>{2}</Motorstoerrelse></getVarianterByTypeIdModelIdAargangMotorstoerrelse></soap12:Body></soap12:Envelope>'.format(modelid, year, motorid)
	response = requests.post(url,data=body,headers=headers)
	trims = xmltodict.parse(response.content)['soap:Envelope']['soap:Body']['getVarianterByTypeIdModelIdAargangMotorstoerrelseResponse']['getVarianterByTypeIdModelIdAargangMotorstoerrelseResult']
	if (trims):
		return trims['KoeretoejModelVariantRegisterRecord']
	else:
		return []

def saveToDb(brand, model, year, engine='', trim='', variantid = '', newvalue = ''):
	print("Brand: " + brand + " Model: " + model + " Year: " + str(year) + "Engine: " + engine + " Trim: " + trim + " Variant: " + variantid + " New: " + newvalue)
	db = pymysql.connect("localhost","root","shogun55","dk" )
	cursor = db.cursor()
	sql = "INSERT INTO autoit2016(make, model, year, motor, trim, variantid, newvalue) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')".format(brand, model, year, engine, trim, variantid, newvalue)
	try:
		cursor.execute(sql)
		db.commit()
	except:
		db.rollback()

	db.close()

def saveToFile(carInfo, brand):
	with open('data/' + brand.replace(' ', '_') + '.json', 'w') as outfile:
		json.dump(carInfo, outfile, sort_keys=True, indent=4)

def getCarInfo(brand, year):
	modelset = getModels(url, headers, brand['FabrikatId'], year)
	if (isinstance(modelset, dict)):
		modelset = [modelset]
	for model in modelset:
		pprint(model)
		if ( isinstance(model, str)):
			print('do nothing: ' + model)
		else:
			print('MODEL: ' + model['ModelNavn'])
			motorset = getMotors(url, headers, model['ModelId'], year)
			if (isinstance(motorset, dict)):
				motorset = [motorset]
			
			for motor in motorset:
				if ( isinstance(motor, str) ) :
					print('do nothing again: ' + motor)
				else:
					trimset = getTrims(url, headers, model['ModelId'], motor['MotorstoerrelseLiterBetegnelse'], year)
					if(isinstance(trimset, dict)):
						trimset = [trimset]
					
					for trim in trimset:
						if(isinstance(trim, str)):
							print('String??? ' + trim)
						else:
							saveToDb(trim['FabrikatNavn'], trim['ModelNavn'], year, trim['MotorstoerrelseLiterBetegnelse'], trim['Variantbetegnelse'], trim['ModelVariantId'], trim['NyprisKR'])

def getAllCarInfo(year):
	print(str(year))	
	for brand in getBrands(url, headers):
		try: 
			print(brand['FabrikatNavn'])
			t = Thread(target=getCarInfo, args=(brand,year))
			t.start()
		except:
			print("Unable to start thread for: %s" % brand)
	

# for brand in getBrands(url, headers):
# 	print(brand["FabrikatNavn"])

# print(getModels(url, headers, 1))
# print(getMotors(url, headers, 164))
# print(getTrims(url, headers, 164, '1,0'))

getAllCarInfo(sys.argv[1])



