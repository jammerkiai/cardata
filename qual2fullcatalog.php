<?php
require('config.php');
require('TrimSanitizer.php');

/*
provider_code	reference_id	brand	model	year	trim	vehicle_type	transmission	brake	doors	passengers	motor	cylinder
*/

$brands = ['ACURA', 'ALFA ROMEO', 'AUDI', 'BMW', 'BUICK', 'CADILLAC',  'CHEVROLET', 'CHRYSLER', 
	'DODGE', 'DODGE RAM', 'FIAT', 'FORD', 'GMC', 'HONDA', 'HUMMER', 'HYUNDAI', 'INFINITI',
	'JAGUAR', 'JEEP', 'KIA', 'LAND ROVER', 'LINCOLN', 'MAZDA', 'MERCEDES BENZ', 
	'MINI', 'MITSUBISHI', 'NISSAN', 'PEUGEOT', 'PONTIAC', 'PORSCHE' , 'RENAULT', 
	'ROVER', 'SAAB', 'SEAT', 'SMART', 'SUBARU', 'SUZUKI', 'TESLA MOTORS',
	'TOYOTA', 'VOLKSWAGEN', 'VOLVO'
	];

foreach ($brands as $brand) {

	$sql = "select * from qual where brand='$brand'";
	$rows = R::getAll($sql);

	foreach ($rows as $row) {

		$fc = R::dispense('catalog');

		$fc->provider_code = 'QUAL';
		$fc->reference_id = $row['qua_id'];
		$fc->brand = $row['brand'];
		$fc->model = $row['model'];
		$fc->year = $row['year'];
		$fc->trim = $row['trim'];
		$fc->vehicle_type = $row['type'];

		$ts = new TrimSanitizer($row['trim']);

		$ts->sanitize();

		$fc->transmission = $ts->getTransmission(); 
		$fc->brake = $ts->getBrake(); 
		$fc->doors = $ts->getDoors(); 
		$fc->passengers = $ts->getPassengers(); 
		$fc->motor = $ts->getMotor(); 
		$fc->cylinder = $ts->getCylinder(); 
		$fc->turbo = $ts->getTurbo();
		$fc->normalizedTrim = $row['model'] . ' ' . $ts->getNormalizedTrim();
		$fc->normalizedModel = $ts->getNormalizedModel();
		R::store($fc);
		unset($fc);
		
		echo "\n" . $row['trim'] . ' -> ' . $ts->getNormalizedTrim();
	}
}




echo "\nDone";