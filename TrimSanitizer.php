<?php

class TrimSanitizer 
{
	/*

	 AUT 
	 	-> TIPTRONIC, *TRONIC
	 
	 STD 
	 	-> DSG, MANUAL, STD., 

	 AC 
	 	-> A/C | CA A/C | 


	*/
	// const TRANSMISSION_REGEX = '/\<AUT[\.]{0,1}\>|[\s]STD[\.]{0,1}|[\s][A-Z]*TRONIC[\s]|[\s]MANUAL[\s]|[\s]CVT[\s]|[\s]ASG[\s]|[\s]ATD[\s]|[\s]DKG[\s]|[\s]DCT[\s]|[\s]DESCONOCIDO[\s]|[\s]DSG[\s]|[\s]EAS[\s]|[\s]GEA[\s]|[\s]HSD[\s]|[\s]TIP[\s]|[\s]SE[\s]/';

	//
	const TRANSMISSION_REGEX = '/AUT[\.,]{0,2}|STD[\.,]{0,2}|MANUAL|[\s]MT[\s]/';


	const BRAKE_REGEX = '/[\s]{1}ABS[\s]{1}/';

	const DOOR_REGEX = '/[0-9]{1,2}P[\s]{1}|[0-9]{1,2}[\s]{0,1}PTAS\.|[0-9]{1,2}[\s]{0,1}PTAS[\s\n]/';

	const PASSENGER_REGEX = '/[0-9]{0,2} OCUP[\.]{0,1}|[0-9]{1,2}P\Z/';

	const MOTOR_REGEX = '/[0-9]{1,2}[\.]{0,1}[0-9]{1,2}[\s]{0,1}[LT]{1}[\Z\s\n]{1}|[0-9]{1,2}\.[0-9]{1,2}[\s\Z]{0,1}/';

	const CYLINDER_REGEX = '/V[0-9]{1,2}|L[0-9]{1,2}/';

	const TURBO_REGEX = '/[\s]TURBO[\s]|[\s]TBO[\s]/';
	
	var $normalizedTrim = '';
	var $normalizedModel = '';

	var $transmission = '';
	var $brake = '';
	var $doors = '';
	var $cylinder = '';
	var $motor = '';
	var $passengers = '';
	var $turbo = '';

	var $skipTurbo = false;

	/**
	* 
	*/

	public function __construct($trim) {
		
		$this->transmission = '';
		$this->brake = '';
		$this->doors = '';
		$this->cylinder = '';
		$this->motor = '';
		$this->passengers = '';
		$this->turbo = '';
		$this->skipTurbo = false;
		$this->normalizedModel = $trim;
		$this->normalizedTrim = $trim;

		return $this;
	}

	public function getTransmission()
	{
		return $this->transmission;
	}

	public function getBrake()
	{
		return $this->brake;
	}

	public function getDoors()
	{
		return $this->doors;
	}

	public function getCylinder()
	{
		return $this->cylinder;
	}

	public function getPassengers()
	{
		return $this->passengers;
	}

	public function getMotor()
	{
		return $this->motor;
	}

	public function getNormalizedTrim()
	{
		return $this->normalizedTrim;
	}

	public function getNormalizedModel()
	{
		return $this->normalizedModel;
	}

	public function getTurbo()
	{
		return $this->turbo;
	}

	public function sanitize() {

		return $this->transmission()
					->brake()
					->doors()
					->passengers()
					->motor()
					->cylinder()
					->turbo();

	}	

	public function transmission() {

		preg_match(self::TRANSMISSION_REGEX, $this->normalizedTrim, $match);
		
		if (!empty($match)) {
			$tmp = str_replace('.', '', trim($match[0]));

			if ($tmp == 'MANUAL' or $tmp == 'MT') {
				$tmp = 'STD';
			} 
			$this->transmission = $tmp;
			$this->normalizedTrim = str_replace( $match[0], ' ' . $this->transmission . ' ', $this->normalizedTrim );
			$this->normalizedModel = str_replace( $match[0], '', $this->normalizedModel); 

		}
		
		return $this;
	}

	public function brake() {

		preg_match(self::BRAKE_REGEX, $this->normalizedTrim, $match);

		if ($match) {
			$this->brake = $match[0];
			$this->normalizedTrim = str_replace( $match[0], $this->brake, $this->normalizedTrim );
			$this->normalizedModel = str_replace( $match[0], '', $this->normalizedModel); 
		}

	 	return $this;
	}

	public function doors() {

		preg_match(self::DOOR_REGEX, $this->normalizedTrim, $match);

		if ($match) {		
			$this->doors = (int) filter_var($match[0], FILTER_SANITIZE_NUMBER_INT) . 'PTAS ';
			$this->normalizedTrim = str_replace( $match[0], $this->doors, $this->normalizedTrim );
			$this->normalizedModel = str_replace( $match[0], '', $this->normalizedModel); 
		}

		return $this;
	}

	 public function passengers() {

		preg_match(self::PASSENGER_REGEX, $this->normalizedTrim, $match);

		if ($match) { 
			$passengers = (int) filter_var($match[0], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$this->passengers = $passengers . 'PAS ';
			$this->normalizedTrim = str_replace( $match[0], $this->passengers, $this->normalizedTrim );
			$this->normalizedModel = str_replace( $match[0], '', $this->normalizedModel); 
		}

		return $this;
	}

	public function motor() {

		preg_match(self::MOTOR_REGEX, $this->normalizedTrim, $match);

		if ($match) {			
			preg_match('/[LT]/', $match[0], $alpha);

			$this->motor = filter_var($match[0], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) . 'L';

			$suffix = ' ';
			if ($alpha) {
				if (strtoupper(trim($alpha[0])) == 'T') {
					$this->skipTurbo = true;
					$this->turbo = 'TURBO';
					$suffix .= 'TURBO ';
				}
			}

			$this->normalizedTrim = str_replace( $match[0], $this->motor . $suffix, $this->normalizedTrim );
			$this->normalizedModel = str_replace( $match[0], '', $this->normalizedModel); 
		}

		return $this;

	}

	public function cylinder() {
		preg_match(self::CYLINDER_REGEX, $this->normalizedTrim, $match);
		if ($match) {
			$this->cylinder = str_replace(' ', '', $match[0]);
			$this->normalizedTrim = str_replace( $match[0], $this->cylinder, $this->normalizedTrim );
			$this->normalizedModel = str_replace( $match[0], '', $this->normalizedModel); 
		}
		return $this;
	}

	public function turbo() {
		if (!$this->skipTurbo) {
			preg_match(self::TURBO_REGEX, $this->normalizedTrim, $match);
			if ($match) {
				$this->turbo = 'TURBO';
				$this->normalizedTrim = str_replace( $match[0], ' ' . $this->turbo . ' ', $this->normalizedTrim );
				$this->normalizedModel = str_replace( 'TURBO', '', $this->normalizedModel); 
			}
		}
		return $this;
	}
	

}