import pymysql


# Open database connection
db = pymysql.connect("localhost","root","shogun55","dk" )

# prepare a cursor object using cursor() method
cursor = db.cursor()

# Prepare SQL query to INSERT a record into the database.
sql = """INSERT INTO car2017(make,
   model, year, motor, trim)
   VALUES ('%s', '%s','%s', '%s','%s')"""
try:
   # Execute the SQL command
   cursor.execute(sql)
   # Commit your changes in the database
   db.commit()
except:
   # Rollback in case there is any error
   db.rollback()

# disconnect from server
db.close()