** Requirements **

python3
python3-pymysql
python3-xmltodict

*** Notes:

** Ubuntu

if python3-pymysql and python3-xmltodict is not found, try to install pip and use that to install

eg.

1. sudo apt-get install pip
2. pip install python3-pymysql

** OSX
use brew to install python3. pip3 will automatically be installed. use pip3 to install requests, pymysql and xmltodic

eg.

1. brew install python3
2. pip3 install requests
3. pip3 install pymysql
4. pip3 install xmltodict


** Description **

- cars.py - grabs data from Next.dk API and saves car info as json files in the data/ folder
- autoit.py -  grabs data from AutoIT API and saves car info into a table with this structure:


```
#!sql

CREATE TABLE `car2017` (
  `id` int(11) UNSIGNED NOT NULL,
  `make` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trim` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variantid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `newvalue` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
)
```

To Run:

1. create folder named data
2. create a database in mysql and update the scripts with the proper credentials
3. run in commandline: `python3 autoit.py`