<?php
require('config.php');
require('TrimSanitizer.php');

/*
provider_code	reference_id	brand	model	year	trim	vehicle_type	transmission	brake	doors	passengers	motor	cylinder
*/

$brands = ['ACURA', 'ALFA ROMEO', 'AUDI', 'BMW', 'GENERAL MOTORS', 'CHRYSLER', 
	'FIAT', 'FORD', 'HONDA', 'HUMMER', 'HYUNDAI', 'JAGUAR', 'JEEP', 'CHRYSLER-JEEP', 'KIA',
	'LAND ROVER', 'MAZDA', 'MERCEDES BENZ', 'MERCURY', 'MITSUBISHI', 'NISSAN', 'PEUGEOT', 
	'PORSCHE' , 'RENAULT', 'MG ROVER', 'SAAB', 'SEAT', 'SMART', 'SUBARU', 'SUZUKI', 'TESLA',
	'TOYOTA', 'VOLKSWAGEN', 'VOLVO'
	];

foreach ($brands as $brand) {

	$sql = "select * from gnps where brand='$brand'";
	$rows = R::getAll($sql);

	foreach ($rows as $row) {

		$fc = R::dispense('catalog');

		$fc->provider_code = 'GNPS';
		$fc->reference_id = $row['gnp_id'];
		$fc->brand = $row['brand'];
		$fc->model = $row['model'];
		$fc->year = $row['year'];
		$fc->trim = $row['trim'];
		$fc->vehicle_type = ( $row['type'] == 'AUT' ) ? 'car' : 'pickup';

		$ts = new TrimSanitizer($row['trim']);
		
		$ts->sanitize();

		$fc->transmission = $ts->getTransmission(); 
		$fc->brake = $ts->getBrake(); 
		$fc->doors = $ts->getDoors(); 
		$fc->passengers = $ts->getPassengers(); 
		$fc->motor = $ts->getMotor(); 
		$fc->cylinder = $ts->getCylinder(); 
		$fc->turbo = $ts->getTurbo();
		$fc->normalizedTrim = $ts->getNormalizedTrim();
		$fc->normalizedModel = $ts->getNormalizedModel();

		R::store($fc);
		unset($fc);

		echo "\n" . $row['trim'] . ' -> ' . $ts->getNormalizedTrim();
	}
}



echo "\nDone";