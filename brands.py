import json
import requests
import sys
from threading import Thread

url = "https://next.dk/api/v2/car/"


def getBrands(url):
	brandUrl = url + "brands"
	response = requests.get(brandUrl)
	brands = response.json()
	carInfo = []
	for brand in brands:	
		print(brand["Name"])
		carInfo.append(brand['Name']) 
	saveToFile(carInfo, "CarBrands")
	print('Done')

def getCarInfo(brand):
	carInfo = {}
	print(brand)
	carInfo[brand] = getModels(url, brand)
	saveToFile(carInfo, brand)		
		
def getModels(url, brand):
	response = requests.get(url + "models?Brand=" + brand)
	models = response.json()
	output = {}
	if "ModelList" in models:
		for modelItem in models["ModelList"]:
			output[modelItem["Name"]] = getYears(url, brand, modelItem["Name"])
	return output

def getYears(url, brand, model):
	response = requests.get(url + "models?Brand=" + brand + "&model=" + model)
	years = response.json()
	output = {}
	for year in years["ModelYearList"]:
		output[year["Name"]] = getEngines(url, brand, model, year["Name"])
	return output

def getEngines(url, brand, model, year):
	response = requests.get(url + "models?Brand=" + brand + "&model=" + model + "&ModelYear=" + year)
	engines = response.json()
	output = {}
	for engine in engines["MotorSizeList"]:
		output[engine["Name"]] = getTrims(url, brand, model, year, engine["Value"])
	return output


def getTrims(url, brand, model, year, engine):
	response = requests.get(url + "models?Brand=" + brand + "&Model=" + model + "&ModelYear=" + year + "&MotorSize=" + engine)
	trims = response.json()
	output = []
	for trim in trims['VariantList']:
		print("Model: " + model + " Year: " + year + " Trim: " + trim["Name"])
		output.append({'trim':trim["Name"], 'variantId' : trim["Value"], 'newValue' : trim["NewValue"]})
	return output


def saveToFile(carInfo, brand):
	with open('data/' + brand.replace(' ', '_') + '.json', 'w') as outfile:
		json.dump(carInfo, outfile, sort_keys=True, indent=4)


def getAllCarInfo():
	with open('data/CarBrands.json', 'r') as data_file:
		brands = json.load(data_file)

	for brand in brands:
		try: 
			t = Thread(target=getCarInfo, args=(brand,))
			t.start()
		except:
			print("Unable to start thread for: %s" % brand)



###
# -> just get all the brands and save to a json file
###
getBrands(url)

###
# get all car info on brands listed in CarBrands.json file
###
#getAllCarInfo()
	

