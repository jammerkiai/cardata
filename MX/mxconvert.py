#!/usr/bin/python3
import json
import sys


def saveToFile(carInfo, brand):
	with open('carinfo/' + removeNonAscii(brand.replace(' ', '_')) + '.json', 'w') as outfile:
		json.dump(carInfo, outfile, sort_keys=True, indent=4)


def removeNonAscii(text):
	return ''.join(i for i in text if ord(i)<128)	

def processCars():
	print('Starting..')
	with open('mx-carlist.json', 'r') as data_file:
		cars = json.load(data_file)
	make = ''
	model = ''
	year = ''
	restruct = {}
	modelobj = {}
	yearobj = {}
	engineobj = {}
	trim = []
	for car in cars['Elemento']:
		if make != car['valor']:
			if make != '':
				print('saving ' + make)
				saveToFile(restruct, make)
				restruct = {}
				modelobj = {}
				yearobj = {}

		make = car['valor']
		model = car['clave2']
		if year != car['clave1']:
			engineobj = {}
			trim = []
		year = car['clave1']
		trim.append({'trim' : car['clave4'], 'newValue' : car['clave3'], 'variantId' : car['clave5']})
		engineobj[0] = trim
		yearobj[year] = engineobj
		modelobj[model] = yearobj
		restruct[make] = modelobj
	saveToFile(restruct, make)
	print('Done.')

def processBrands():
	print('Starting..')
	with open('mx-carlist.json', 'r') as data_file:
		cars = json.load(data_file)
	brands = []
	make = ''
	for car in cars['Elemento']:
		if make != car['valor']:
			make = car['valor']
			print(make)
			brands.append(make)
	saveToFile(brands, 'CarBrands')


processBrands()
processCars()
