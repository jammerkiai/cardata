<?php
require('config.php');
require('TrimSanitizer.php');

/*
provider_code	reference_id	brand	model	year	trim	vehicle_type	transmission	brake	doors	passengers	motor	cylinder
*/

$brands = ['ACURA', 'ALFA ROMEO', 'AUDI', 'BMW', 'BUICK', 'CADILLAC', 'CHEVROLET', 'CHRYSLER', 'DODGE', 
	'FIAT', 'FORD', 'GMC', 'HONDA', 'HUMMER', 'HYUNDAI', 'INFINITI', 'JAGUAR', 'JEEP', 'KIA',
	'LAND ROVER', 'LINCOLN', 'MAZDA', 'MERCEDES BENZ', 'MERCURY', 'MITSUBISHI', 'NISSAN', 'PEUGEOT', 
	'PONTIAC', 'PORSCHE' , 'RENAULT', 'ROVER', 'SAAB', 'SEAT', 'SMART', 'SUBARU', 'SUZUKI', 'TESLA MOTORS',
	'TOYOTA', 'VOLKSWAGEN', 'VOLVO'
	];

foreach ($brands as $brand) {
	$sql = "select a.* from hdicatalograw a where a.marca=
			( select DISTINCT(b.brandid) from hdis b where b.brand='$brand' )";

	$rows = R::getAll($sql);

	foreach ($rows as $row) {

		$fc = R::dispense('catalog');

		$fc->provider_code = 'HDIS';
		$fc->reference_id = $row['clave_vehiculo'];
		$fc->brand = $brand;
		$fc->model = $row['tipo'];
		$fc->year = $row['ano'];
		$fc->trim = $row['version'];
		$fc->vehicle_type = ( $row['tipo_vehiculo'] == 4579 ) ? 'car' : 'pickup';

		$transmission = ( $row['transmision'] == 4534 ) ? 'STD' : 'AUT';
		$fc->transmission = $transmission;

		$brake = ( $row['frenos'] == 3870 ) ? 'ABS' : ''; 
		$fc->brake = $brake;

		$doors = $row['puertas'] . 'PTAS'; 
		$fc->doors = $doors;

		$passengers = $row['ocupantes'] . 'PAS';
		$fc->passengers =  $passengers;

		$cylinder =$row['cilindros'];
		$fc->cylinder = $cylinder;

		$ts = new TrimSanitizer($row['version']);
		$ts->sanitize();

		$fc->motor = $ts->getMotor(); 
		$fc->turbo = $ts->getTurbo();

		$normalizedTrim = $row['tipo'] . ' ' . $ts->getNormalizedTrim() . ' ' . $transmission . ' ' . $brake . ' ' . $doors . ' ' .
			$passengers . ' ' . $cylinder . ' ';
		$fc->normalizedTrim = $normalizedTrim;
		$fc->normalizedModel = $ts->getNormalizedModel();

		R::store($fc);
		unset($fc);
		

		echo "\n" . $row['version'] . ' -> ' . $normalizedTrim;
	}
}






echo "\nDone";