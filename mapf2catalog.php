<?php
require('config.php');
require('TrimSanitizer.php');

/*
provider_code	reference_id	brand	model	year	trim	vehicle_type	transmission	brake	doors	passengers	motor	cylinder
*/

$brands = ['ACURA', 'ALFA ROMEO', 'AUDI', 'BMW', 'CHEVROLET', 'CHRYSLER-DODGE', 'FIAT', 'FORD', 
	'GMC', 'HONDA', 'HYUNDAI', 'INFINITI', 'JAGUAR', 'KIA', 'LAND ROVER', 'MAZDA', 'MERCEDES BENZ', 
	'MINI', 'MITSUBISHI', 'NISSAN', 'PEUGEOT', 'PORSCHE' , 'RENAULT', 'ROVER', 
	'SAAB', 'SEAT', 'SUBARU', 'SUZUKI', 'TOYOTA', 'VOLKSWAGEN', 'VOLVO'];

foreach ($brands as $brand) {
	$sql = "select * from mapf where brand='$brand'";
	$rows = R::getAll($sql);

	foreach ($rows as $row) {

		$fc = R::dispense('catalog');

		$fc->provider_code = 'MAPF';

		$brandId = str_pad($row['brand_id'], 3, '0', STR_PAD_LEFT);
		$modelId = str_pad($row['model_id'], 3, '0', STR_PAD_LEFT); 
		$typeId = ($row['type'] == 'car') ? '01' : '12';
		$refId =  $brandId . $modelId . $typeId; 

		$ts = new TrimSanitizer($row['trim']);
		$ts->sanitize();

		$fc->reference_id = $refId;
		$fc->brand = $row['brand'];
		$fc->model = $row['trim'];
		$fc->year = $row['year'];
		$fc->trim = $row['trim'];
		$fc->vehicle_type = $row['type'];
		$fc->transmission = $ts->getTransmission(); 
		$fc->brake = $ts->getBrake(); 
		$fc->doors = $ts->getDoors(); 
		$fc->passengers = $ts->getPassengers(); 
		$fc->motor = $ts->getMotor(); 
		$fc->cylinder = $ts->getCylinder(); 
		$fc->turbo = $ts->getTurbo();
		$fc->normalizedTrim = $ts->getNormalizedTrim();
		$fc->normalizedModel = $ts->getNormalizedModel();

		R::store($fc);
		unset($fc);
		
		echo "\n" . $row['trim'] . ' -> ' . $ts->getNormalizedTrim();
	}
}





echo "\nDone";